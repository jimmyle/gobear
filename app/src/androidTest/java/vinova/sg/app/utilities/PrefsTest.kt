package vinova.sg.app.utilities

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PrefsTest {

    private lateinit var context: Context
    private lateinit var prefs: Prefs
    @Before
    fun init() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        prefs = Prefs(context)
    }

    @Test
    fun getUserNameTest() {
        val usernameTest = "usernameTest"
        prefs.username = usernameTest
        assertEquals(usernameTest, prefs.username)
    }

    @Test
    fun getTokenTest() {
        val usernameTest = "usernameTest"
        val passwordTest = "passwordTest"
        val tokenTest = usernameTest + passwordTest

        prefs.token = tokenTest

        assertEquals(tokenTest, prefs.token)
    }

    @Test
    fun getIsRememberTest() {
        val isRememberTest = true

        prefs.isRemember = isRememberTest

        assertEquals(isRememberTest, prefs.isRemember)
    }

    @Test
    fun getSaveAllData() {
        val usernameTest = "usernameTest"
        val passwordTest = "passwordTest"
        val isRememberTest = true
        val tokenTest = usernameTest + passwordTest

        prefs.username = usernameTest
        prefs.token = tokenTest
        prefs.isRemember = isRememberTest

        assertEquals(usernameTest, prefs.username)
        assertEquals(tokenTest, prefs.token)
        assertEquals(isRememberTest, prefs.isRemember)
    }

    @Test
    fun getClearAllData() {
        getSaveAllData()

        prefs.clearAllData()
        assertEquals("", prefs.username)
        assertEquals("", prefs.token)
        assertEquals(false, prefs.isRemember)
    }

    @After
    fun destroy() {
        prefs.clearAllData()
    }
}