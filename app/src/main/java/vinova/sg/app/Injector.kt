/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vinova.sg.app

import android.content.Context
import vinova.sg.app.repository.features.RssRepository
import vinova.sg.app.repository.inMemory.byItem.InMemoryByItemRssRepository
import vinova.sg.app.viewmodel.auth.AuthViewModelFactory
import vinova.sg.app.viewmodel.rss.NewsViewModelFactory

/**
 * Static methods used to inject classes needed for various Activities and Fragments.
 */
object Injector {

    fun provideAuthViewModelFactory(): AuthViewModelFactory {
        return AuthViewModelFactory()
    }

    fun provideServiceViewModelFactory(
            context: Context, repoType: RssRepository.Type
    ): NewsViewModelFactory<Any> {
        val repo = IServiceLocator.instance(context)
                .getRssRepository(repoType)
        if (repo is InMemoryByItemRssRepository)
            return NewsViewModelFactory(repo as RssRepository.ByItem)
        else
            return NewsViewModelFactory(repo as RssRepository.ByObject)
    }
}

