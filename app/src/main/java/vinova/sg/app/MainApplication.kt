package vinova.sg.app

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner

open class MainApplication : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()
        instance = this

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    companion object {
        var instance = MainApplication()
            private set
        var isActive: Boolean = false
    }

    /*fun saveCurrentUser(user: CustomerAuthentication?) {
        saveUserPrefObj(user)
    }


    fun updateCurrentUser() {
        currentUser = getUserPrefObj()
    }

    fun updateCurrentProvider() {
        currentProvider = ProviderPrefUtil.profile
    }*/

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isActive = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        isActive = true
    }
}