package vinova.sg.app.exception

import sg.vinova.rssconverterfactory.BaseResponse
import vinova.sg.app.constant.CONNECT_ERROR
import vinova.sg.app.constant.REQUEST_TIMEOUT
import vinova.sg.app.constant.SERVER_ERROR
import vinova.sg.app.model.BaseResponsePagedList

class NetworkErrorException(message: String?, val code: Int? = null) : Exception(message) {
    companion object {
        val DEFAULT_ERROR = NetworkErrorException(SERVER_ERROR)
        val NO_NETWORK = NetworkErrorException(REQUEST_TIMEOUT)
        val NO_CONNECT_TO_SERVER = NetworkErrorException(CONNECT_ERROR)
        fun newWith(response: BaseResponse?): NetworkErrorException =
                response?.let {
                    NetworkErrorException(it.message, it.code)
                } ?: DEFAULT_ERROR

        fun newWith(response: BaseResponsePagedList<*>?): NetworkErrorException =
                response?.message?.let {
                    NetworkErrorException(it)
                } ?: DEFAULT_ERROR
    }
}