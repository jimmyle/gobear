package vinova.sg.app.constant

const val SERVER_ERROR = "Server error"
const val INTERNET_ERROR = "No internet connection"
const val CONNECT_ERROR = "No connect to server"
const val REQUEST_TIMEOUT = "Request Timeout"

const val PAGE_SIZE = 25