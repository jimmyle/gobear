package vinova.sg.app.viewmodel.auth

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import vinova.sg.app.ext.checkIsNotEmpty
import vinova.sg.app.ext.checkPassword
import vinova.sg.app.model.feature.auth.User

class AuthViewModel : ViewModel() {
    private val loginRequest = MutableLiveData<Pair<String, String>>()

    val userLiveData: LiveData<User>

    var username = ObservableField<String>("")
    var password = ObservableField<String>("")

    init {
        userLiveData = switchMap<Pair<String, String>, User>(loginRequest) {
            val response = MutableLiveData<User>()
            val user = checkValidAccount(it)
            response.value = user
            return@switchMap response
        }
    }

    fun checkValidAccount(it: Pair<String, String>): User {
        val user = User()
        when {
            !it.second.checkPassword() -> {
                user.message = "Password is too short (Minimum is 6 characters)"
                user.status = false
            }

            it.first.equals("GoBear", true) && it.second.equals("GoBearDemo") -> {
                user.username = it.first
                user.token = it.first + it.second
                user.status = true
                user.message = "Login success."
            }

            else -> {
                user.status = false
                user.message = "Wrong username or password, please try again."
            }
        }
        return user
    }

    fun login(username: String, password: String) {
        this.loginRequest.value = Pair(username, password)
    }

    fun setActive(username: String, password: String) = username.checkIsNotEmpty() && password.checkIsNotEmpty()
}