package vinova.sg.app.viewmodel.rss

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import vinova.sg.app.repository.features.RssRepository

class NewsViewModelFactory<T>(
        private val repository: T
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            when (modelClass) {
                GetNewsViewModel::class.java -> GetNewsViewModel(repository as RssRepository.ByObject) as T
                else -> GetNewsViewModel(repository as RssRepository.ByObject) as T
            }
}