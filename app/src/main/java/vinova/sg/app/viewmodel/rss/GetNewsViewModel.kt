package vinova.sg.app.viewmodel.rss

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import sg.vinova.rssconverterfactory.RssItem
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.ext.convertToPagedList
import vinova.sg.app.model.feature.news.News
import vinova.sg.app.repository.NetworkState
import vinova.sg.app.repository.features.RssRepository
import vinova.sg.app.utilities.AbsentLiveData

class GetNewsViewModel(private val repository: RssRepository.ByObject) : ViewModel() {
    private val requestGetRss = MutableLiveData<Any>()

    val errorLiveData = MutableLiveData<NetworkErrorException>()
    val networkState = MutableLiveData<NetworkState>()

    private val responseRssItemData: LiveData<List<RssItem?>?>

    val newsLiveData: LiveData<PagedList<News>>

    init {
        responseRssItemData = switchMap(requestGetRss) {
            if (it == null) {
                AbsentLiveData.create()
            } else {
                repository.getRssItem(error = errorLiveData, networkState = networkState)
            }
        }

        newsLiveData = switchMap<List<RssItem?>?, PagedList<News>>(responseRssItemData) { it ->
            val result = it?.map {
                News(title = it?.title,
                        link = it?.link,
                        image = it?.image,
                        description = it?.description,
                        publishDate = it?.publishDate)
            }
            result?.convertToPagedList()
        }
    }

    fun getRssData() {
        requestGetRss.value = Any()
    }
}