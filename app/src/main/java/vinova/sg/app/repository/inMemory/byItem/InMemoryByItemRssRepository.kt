/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vinova.sg.app.repository.inMemory.byItem

import androidx.lifecycle.MutableLiveData
import sg.vinova.rssconverterfactory.RssItem
import vinova.sg.app.api.GoBearApi
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.repository.Listing
import vinova.sg.app.repository.NetworkState
import vinova.sg.app.repository.features.RssRepository
import java.util.concurrent.Executor

/**
 * Repository implementation that returns a Listing that loads data directly from the network
 * and uses the Item's name as the key to discover prev/next pages.
 */
class InMemoryByItemRssRepository(
        private val apiService: GoBearApi,
        private val networkExecutor: Executor) : RssRepository.ByItem {

    override fun getRss(request: MutableLiveData<Listing<RssItem>>, pageSize: Int,
                        error: MutableLiveData<NetworkErrorException>?,
                        networkState: MutableLiveData<NetworkState>?,
                        refreshState: MutableLiveData<NetworkState>?) {

//        networkExecutor.execute {
//            apiService.getNewsFeed().enqueue(
//                    RetrofitByItemLiveData(
//                            requestLiveData = request,
//                            error = error, networkState = networkState, refreshState = refreshState,
//                            pageSize = pageSize,
//                            call = apiService.getNewsFeed()))
//        }

    }
}

