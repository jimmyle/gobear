package vinova.sg.app.repository.inMemory.byObject

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.rssconverterfactory.BaseResponse
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.repository.NetworkState
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class RetrofitByObjectLiveData<T : BaseResponse, K : BaseResponse>(private val call: Call<T>,
                                                                        private val error: MutableLiveData<NetworkErrorException>? = null,
                                                                        private val networkState: MutableLiveData<NetworkState>? = null,
                                                                        private var retry: Retry<K>? = null,
                                                                        private var refresh: MutableLiveData<(() -> Unit)>? = null) : MutableLiveData<T>(), Callback<T> {
    override fun onActive() {
        //Default is main thread
        if (!call.isCanceled && !call.isExecuted) {
            networkState?.postValue(NetworkState.LOADING)
            call.clone().enqueue(this)
        }
    }

//    fun run() {
//        if (refresh != null) {
//            refresh?.refreshFunc =
//                    {
//                        networkState?.postValue(NetworkState.LOADING)
//                        call.clone().enqueue(this@RetrofitByObjectLiveData)
//                    }
//        }
//    }

    override fun onFailure(executedCall: Call<T>?, t: Throwable?) {
        networkState?.postValue(NetworkState.error(t?.message ?: "unknown err"))
        // networkState?.postValue(NetworkState.LOADING)
        if (t is UnknownHostException || t is SocketTimeoutException) {
            //networkState?.postValue(NetworkState.LOADED)
            error?.postValue(NetworkErrorException.NO_NETWORK)
            return
        }
        if (t is ConnectException) {
            //networkState?.postValue(NetworkState.LOADED)
            error?.postValue(NetworkErrorException.NO_CONNECT_TO_SERVER)
            return
        }
        error?.postValue(NetworkErrorException(t?.message, 9999))
    }

    override fun onResponse(executedCall: Call<T>?, response: Response<T>?) {
        if ((response?.isSuccessful == true)) {
            networkState?.postValue(NetworkState.LOADED)
            postValue(response.body())
//            refresh?.postValue { onActive() }
            call.cancel()
            retry?.retryFunc = null
            call.cancel()
        } else {
            networkState?.postValue(NetworkState.error(response?.body()?.message ?: "unknown err"))
            //networkState?.postValue(NetworkState.LOADING)

            error?.postValue(NetworkErrorException.newWith(response?.body()))
        }
    }

    fun cancel() = if (!call.isCanceled) call.cancel() else Unit
    open class Retry<K>(var retryFunc: Call<K>? = null)
//    class Refresh(var refreshFunc: (() -> Any?)? = null)
}
