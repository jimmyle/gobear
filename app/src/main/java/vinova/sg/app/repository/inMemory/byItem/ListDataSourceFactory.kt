package vinova.sg.app.repository.inMemory.byItem

import androidx.paging.DataSource

class ListDataSourceFactory<T>(val list: List<T>?) : DataSource.Factory<Int, T>() {
    override fun create(): DataSource<Int, T> = ListDataSource<T>(list ?: emptyList())
}