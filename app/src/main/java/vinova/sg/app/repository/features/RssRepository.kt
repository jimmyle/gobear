package vinova.sg.app.repository.features

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import sg.vinova.rssconverterfactory.RssItem
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.repository.Listing
import vinova.sg.app.repository.NetworkState

class RssRepository {

    interface ByObject {
        fun getRssItem(error: MutableLiveData<NetworkErrorException>? = null,
                       networkState: MutableLiveData<NetworkState>? = null): LiveData<List<RssItem?>>
    }

    interface ByItem {
        fun getRss(request: MutableLiveData<Listing<RssItem>>, pageSize: Int,
                   error: MutableLiveData<NetworkErrorException>? = null,
                   networkState: MutableLiveData<NetworkState>? = null,
                   refreshState: MutableLiveData<NetworkState>? = null)
    }

    enum class Type {
        IN_MEMORY_BY_ITEM,
        IN_MEMORY_BY_OBJECT
    }
}