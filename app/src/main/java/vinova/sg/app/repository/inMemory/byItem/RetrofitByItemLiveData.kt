package vinova.sg.app.repository.inMemory.byItem

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.rssconverterfactory.BaseResponse
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.model.BaseResponsePagedList
import vinova.sg.app.repository.Listing
import vinova.sg.app.repository.NetworkState
import java.net.UnknownHostException

open class RetrofitByItemLiveData<I, T : BaseResponse>(private val requestLiveData: MutableLiveData<Listing<I>>,
                                                       private val error: MutableLiveData<NetworkErrorException>? = null,
                                                       private val networkState: MutableLiveData<NetworkState>? = null,
                                                       private val refreshState: MutableLiveData<NetworkState>? = null,
                                                       private val pageSize: Int,
                                                       private val call: Call<T>) : MutableLiveData<T>(), Callback<T> {

    override fun onActive() {
        if (!call.isCanceled && !call.isExecuted) {
            call.clone().enqueue(this)
        }
    }

    override fun onFailure(executedCall: Call<T>?, t: Throwable?) {
        networkState?.postValue(NetworkState.error(t?.message ?: "unknown err"))
        // networkState?.postValue(NetworkState.LOADING)
        if (t is UnknownHostException) {
            error?.postValue(NetworkErrorException.NO_NETWORK)
            refreshState?.postValue(NetworkState.LOADED)
            return
        }
        error?.postValue(NetworkErrorException(t?.message))
    }

    override fun onResponse(executedCall: Call<T>?, response: Response<T>?) {
        if (response?.isSuccessful == true && (response.body() as? BaseResponsePagedList<*>)?.isSuccess() == true) {
            convertType(response)
            networkState?.postValue(NetworkState.LOADED)
            refreshState?.postValue(NetworkState.LOADED)
        } else if (response?.body() == null) {
            networkState?.postValue(NetworkState.error(response?.message()))
        } else {
            networkState?.postValue(NetworkState.error((response.body() as? BaseResponsePagedList<*>)?.message
                    ?: "unknown err"))
            networkState?.postValue(NetworkState.LOADING)
        }
    }

    fun cancel() = if (!call.isCanceled) call.cancel() else Unit

    private fun convertType(response: Response<T>) {
        val sourceFactory = ListDataSourceFactory((response.body() as? BaseResponsePagedList<*>)?.data)
        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(pageSize)
                .setPageSize(pageSize)
                .build()
        val pagedList = LivePagedListBuilder(sourceFactory, pagedListConfig)
                .build()

        val result = Listing(
                pagedList = pagedList as LiveData<PagedList<I>>,
                retry = { onActive() },
                refresh = { onActive() })

        (requestLiveData).postValue(result)
    }
}
