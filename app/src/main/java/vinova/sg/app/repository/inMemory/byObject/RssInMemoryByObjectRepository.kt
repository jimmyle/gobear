package vinova.sg.app.repository.inMemory.byObject

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import sg.vinova.rssconverterfactory.RssItem
import vinova.sg.app.api.GoBearApi
import vinova.sg.app.exception.NetworkErrorException
import vinova.sg.app.repository.NetworkState
import vinova.sg.app.repository.features.RssRepository

open class RssInMemoryByObjectRepository(private val apiService: GoBearApi) : RssRepository.ByObject {

    override fun getRssItem(error: MutableLiveData<NetworkErrorException>?,
                            networkState: MutableLiveData<NetworkState>?): LiveData<List<RssItem?>> =
            map(RetrofitByObjectLiveData(
                    apiService.getNews(),
                    error = error, networkState = networkState,
                    retry = RetrofitByObjectLiveData.Retry(apiService.getNews()))) { it.data }
}