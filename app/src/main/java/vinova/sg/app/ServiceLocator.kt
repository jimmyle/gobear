/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vinova.sg.app

import android.app.Application
import vinova.sg.app.api.GoBearApi
import vinova.sg.app.repository.features.RssRepository
import vinova.sg.app.repository.inMemory.byItem.InMemoryByItemRssRepository
import vinova.sg.app.repository.inMemory.byObject.RssInMemoryByObjectRepository
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * default implementation of ServiceLocator that uses production endpoints.
 */
open class DefaultServiceLocator(val app: Application, val useInMemoryDb: Boolean) : IServiceLocator {

    // thread pool used for disk access
    @Suppress("PrivatePropertyName")
    private val DISK_IO = Executors.newSingleThreadExecutor()

    // thread pool used for network requests
    @Suppress("PrivatePropertyName")
    private val NETWORK_IO = Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)))

    private val api by lazy {
        GoBearApi.create(getNetworkExecutor())
    }

    override fun getNetworkExecutor(): Executor = this.NETWORK_IO
    override fun getDiskIOExecutor(): Executor = DISK_IO
    override fun getGoBearApi(): GoBearApi = api

    override fun getRssRepository(type: RssRepository.Type): Any =
            when (type) {
                RssRepository.Type.IN_MEMORY_BY_ITEM -> InMemoryByItemRssRepository(
                        apiService = getGoBearApi(),
                        networkExecutor = getNetworkExecutor()
                )

                RssRepository.Type.IN_MEMORY_BY_OBJECT -> RssInMemoryByObjectRepository(
                        apiService = getGoBearApi()
                )
            }
}