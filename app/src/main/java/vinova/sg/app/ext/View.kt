package vinova.sg.app.ext

import android.view.View
import androidx.core.view.isVisible

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.isVisible = false
}

fun View.visible() {
    this.isVisible = true
}