package vinova.sg.app.ext

import android.util.Patterns

fun String.checkIsNotEmpty(): Boolean = this.trim().isNotEmpty()
fun String.checkPhoneNumber(): Boolean = this.trim().length > 7 && Patterns.PHONE.matcher(this).matches()
fun String.checkEmail(): Boolean = this.trim().isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this.trim()).matches()
fun String.checkPassword(): Boolean = this.trim().length > 5
fun String.checkDate(): Boolean = this.checkIsNotEmpty()
        && this.trim().length == 10
        && this.subSequence(0..1).toString().toInt() in 1..31
        && this.subSequence(3..4).toString().toInt() in 1..12
