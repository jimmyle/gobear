package vinova.sg.app.ext

import android.annotation.SuppressLint
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import vinova.sg.app.repository.inMemory.byItem.ListDataSourceFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

fun <T> List<T>.convertToPagedList(): LiveData<PagedList<T>> {
    val sourceFactory = ListDataSourceFactory(this)
    val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(25)
            .setPageSize(25)
            .build()
    return LivePagedListBuilder(sourceFactory, pagedListConfig)
            .build()
}

@SuppressLint("WrongThread")
@MainThread
fun <T> List<T>.convertToNormalPagedList(executor: Executor): PagedList<T> {
    val sourceFactory = ListDataSourceFactory(this)
    val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(25)
            .setPageSize(25)
            .build()
    return PagedList.Builder(sourceFactory.create(), pagedListConfig)
            .setFetchExecutor(Executors.newSingleThreadExecutor())
            .setNotifyExecutor(executor).build()
}
