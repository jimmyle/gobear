/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vinova.sg.app.api

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import sg.vinova.rssconverterfactory.RssConverterFactory
import sg.vinova.rssconverterfactory.RssFeed
import vinova.sg.app.BuildConfig
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

interface GoBearApi {

    @GET("/news/world/asia/rss.xml")
    fun getNews(): Call<RssFeed>

    companion object {
        private const val BASE_URL = BuildConfig.SERVER_URL
        fun create(executor: Executor): GoBearApi = create(executor, HttpUrl.parse(BASE_URL)!!)
        fun create(executor: Executor, httpUrl: HttpUrl): GoBearApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Log.d("API", it)
            })
            logger.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)


            httpClient.addInterceptor { chain ->
                val original = chain.request()

                val requestBuilder: Request.Builder?
                requestBuilder =
                        original.newBuilder()

                val request = requestBuilder?.build()!!
                chain.proceed(request)
            }
            if (BuildConfig.DEBUG) {
                httpClient.addNetworkInterceptor(StethoInterceptor())
            }
            return Retrofit.Builder()
                    .baseUrl(httpUrl)
                    .client(httpClient.build())
                    .addConverterFactory(RssConverterFactory.create())
                    .callbackExecutor(executor)
                    .build()
                    .create(GoBearApi::class.java)
        }
    }
}