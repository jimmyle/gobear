@file:Suppress("NOTHING_TO_INLINE")

package vinova.sg.app.utilities

import android.animation.ObjectAnimator
import android.animation.StateListAnimator
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import org.jetbrains.anko.find
import vinova.sg.app.R

inline fun setupToolbar(appCompatActivity: AppCompatActivity, title: String, hasBack: Boolean = false) {
    appCompatActivity.setSupportActionBar(appCompatActivity.find(R.id.toolbar))
    appCompatActivity.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
    appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(hasBack)
    appCompatActivity.supportActionBar?.setDisplayShowTitleEnabled(false)
    appCompatActivity.find<AppCompatTextView>(R.id.tvTitleToolbar).text = title
    appCompatActivity.find<Toolbar>(R.id.toolbar).apply {
        setPadding(resources.getDimensionPixelOffset(R.dimen.no_padding), 0, 0, 0)
    }

    val layoutAppBar = appCompatActivity.find<AppBarLayout>(R.id.appBarLayout)

    val stateListAnimator = StateListAnimator()
    stateListAnimator.addState(IntArray(0), ObjectAnimator.ofFloat(layoutAppBar, "elevation", 6F))
    layoutAppBar.stateListAnimator = stateListAnimator

    appCompatActivity.find<Toolbar>(R.id.toolbar).setNavigationOnClickListener {
        appCompatActivity.onBackPressed()
    }
}