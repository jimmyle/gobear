package vinova.sg.app.utilities

import android.content.Context
import android.content.SharedPreferences

class Prefs(val context: Context) {
    companion object {
        const val PREFS_FILENAME = "gopear.prefs"

        private const val USERNAME = "username"
        private const val TOKEN = "token"
        private const val IS_REMEMBER = "is_remember"
    }

    private var prefs: SharedPreferences

    init {
        prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
    }

    var username: String?
        get() = prefs.getString(USERNAME, "")
        set(value) = prefs.edit().putString(USERNAME, value).apply()

    var token: String?
        get() = prefs.getString(TOKEN, "")
        set(value) = prefs.edit().putString(TOKEN, value).apply()

    var isRemember: Boolean?
        get() = prefs.getBoolean(IS_REMEMBER, false)
        set(value) = prefs.edit().putBoolean(IS_REMEMBER, value ?: false).apply()


    fun saveData(username: String?, token: String?, isRemember: Boolean?) {
        this.username = username
        this.token = token
        this.isRemember = isRemember
    }

    fun clearAllData() = prefs.edit().clear().commit()
}