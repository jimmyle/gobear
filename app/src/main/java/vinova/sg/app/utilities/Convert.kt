@file:Suppress("NOTHING_TO_INLINE")

package vinova.sg.app.utilities

import java.text.SimpleDateFormat
import java.util.*

inline fun convertIntToString(date: Int, month: Int, year: Int, pattern: String = "dd/MM/yyyy"): String {
    val fommater = SimpleDateFormat(pattern, Locale.US)
    val calendar = Calendar.getInstance()
    calendar.set(year, month, date)
    val destination = calendar.time
    return fommater.format(destination)
}