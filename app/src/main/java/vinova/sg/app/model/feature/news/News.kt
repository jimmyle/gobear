package vinova.sg.app.model.feature.news

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class News(
        var title: String? = null,
        var link: String? = null,
        var image: String? = null,
        var publishDate: String? = null,
        var description: String? = null
) : Parcelable