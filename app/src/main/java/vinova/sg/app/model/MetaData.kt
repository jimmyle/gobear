package vinova.sg.app.model

data class MetaData(
        val total_count: Int?,
        val total_pages: Int?,
        val next_page: Int?,
        val prev_page: Int?,
        val current_page: Int?,
        val current_per_page: Int?)