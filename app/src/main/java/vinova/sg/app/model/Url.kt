package vinova.sg.app.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Url(
        var thumb: String? = "",
        var medium: String? = "",
        var original: String? = ""
) : Parcelable