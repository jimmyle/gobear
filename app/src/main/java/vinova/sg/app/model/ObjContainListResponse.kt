package vinova.sg.app.model


abstract class ObjContainListResponse<T>(
        var list: List<T>
)