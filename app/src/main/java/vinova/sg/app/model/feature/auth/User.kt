package vinova.sg.app.model.feature.auth

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import sg.vinova.rssconverterfactory.BaseResponse

@Parcelize
data class User(
        var username: String? = "",
        var token: String? = null
) : BaseResponse(), Parcelable