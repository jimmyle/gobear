package vinova.sg.app.model

abstract class BaseResponsePagedList<T>(val status: Boolean? = null,
                                        val message: String? = null,
                                        val metadata: MetaData? = null,
                                        val code: Int? = null,
                                        open val data: List<T>? = null) {
    fun isSuccess() = status == true
}