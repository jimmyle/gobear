package vinova.sg.app.model

abstract class BaseResponsePagedListObject<T, E : ObjContainListResponse<T>>(val status: Boolean? = null,
                                                                             val message: String? = null,
                                                                             val metadata: MetaData? = null,
                                                                             val code: Int? = null,
                                                                             open val data: E? = null) {
    fun isSuccess() = status == true
}