package vinova.sg.app.viewmodel.rss

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import sg.vinova.rssconverterfactory.RssItem
import vinova.sg.app.model.feature.news.News
import vinova.sg.app.repository.inMemory.byObject.RssInMemoryByObjectRepository
import vinova.sg.app.viewmodel.util.testObserverObject

class GetNewsViewModelTest {

    private lateinit var viewModel: GetNewsViewModel
    private lateinit var rssInMemoryByObjectRepository: RssInMemoryByObjectRepository
    private var mockRss = arrayListOf<RssItem>()
    private var mockNews = arrayListOf<News>()

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun init() {
        rssInMemoryByObjectRepository = Mockito.mock(RssInMemoryByObjectRepository::class.java)
        viewModel = GetNewsViewModel(rssInMemoryByObjectRepository)

        for (i in 1..9) {
            val rssItemTest = RssItem().apply {
                title = "Title Test $i"
                description = "Description Test $i"
                image = "Image Test $i"
                link = "Link Test $i"
                publishDate = "Publish Date Test $i"
            }
            mockRss.add(rssItemTest)

            val news = News().apply {
                title = "Title Test $i"
                description = "Description Test $i"
                image = "Image Test $i"
                link = "Link Test $i"
                publishDate = "Publish Date Test $i"
            }
            mockNews.add(news)
        }
    }

    @Test
    fun `Get Rss Data success and return object`() {
        val mutableLiveDataTest = MutableLiveData<List<RssItem?>>().apply { value = mockRss }
        val liveDataTest: LiveData<List<RssItem?>> = map(mutableLiveDataTest) { it }

        `when`(rssInMemoryByObjectRepository.getRssItem(viewModel.errorLiveData, viewModel.networkState)).thenReturn(liveDataTest)
        val newsLiveData = viewModel.newsLiveData.testObserverObject()

        viewModel.getRssData()
        assertEquals(mockNews.size, newsLiveData.observedValues?.size)
        assertEquals(mockNews.toString(), newsLiveData.observedValues?.toString())
    }

    @Test
    fun `Get Rss Data with empty data data`() {
        val mutableLiveDataTest = MutableLiveData<List<RssItem?>>().apply { value = listOf() }
        val liveDataTest: LiveData<List<RssItem?>> = map(mutableLiveDataTest) { it }

        `when`(rssInMemoryByObjectRepository.getRssItem(viewModel.errorLiveData, viewModel.networkState)).thenReturn(liveDataTest)
        val newsLiveData = viewModel.newsLiveData.testObserverObject()

        viewModel.getRssData()
        assertEquals(0, newsLiveData.observedValues?.size)
        assertEquals(liveDataTest.value.toString(), newsLiveData.observedValues?.toString())
    }
}