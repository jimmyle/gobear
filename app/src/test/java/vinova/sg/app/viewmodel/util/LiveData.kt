package vinova.sg.app.viewmodel.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

class TestObserver<T> : Observer<T> {

    val observedValues = mutableListOf<T?>()

    override fun onChanged(value: T?) {
        observedValues.add(value)
    }
}

class TestObserverObject<T> : Observer<T> {

    var observedValues: T? = null

    override fun onChanged(value: T?) {
        observedValues = value
    }
}

fun <T> LiveData<T>.testObserver() = TestObserver<T>().also {
    observeForever(it)
}

fun <T> LiveData<T>.testObserverObject() = TestObserverObject<T>().also {
    observeForever(it)
}