package vinova.sg.app.viewmodel.auth

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.junit.MockitoJUnit
import vinova.sg.app.model.feature.auth.User
import vinova.sg.app.viewmodel.util.testObserverObject

class AuthViewModelTest {

    private lateinit var viewModel: AuthViewModel

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun init() {
        viewModel = AuthViewModel()
    }

    @Test
    fun `Active equals false when login with username & password empty`() {
        val usernameTest = ""
        val passwordTest = ""
        assertFalse(viewModel.setActive(usernameTest, passwordTest))
    }

    @Test
    fun `Active equals false when login with username & password not empty`() {
        val usernameTest = "test"
        val passwordTest = "test"
        assertTrue(viewModel.setActive(usernameTest, passwordTest))
    }

    @Test
    fun `show message when check valid account with length of password smaller 6 `() {
        val usernameTest = ""
        val passwordTest = "12345"
        val user = viewModel.checkValidAccount(Pair(usernameTest, passwordTest))
        assertTrue(user.status == false)
        assertEquals("Password is too short (Minimum is 6 characters)", user.message)
    }

    @Test
    fun `show message when check valid account with wrong username or password `() {
        val usernameTest = "test"
        val passwordTest = "123456"
        val user = viewModel.checkValidAccount(Pair(usernameTest, passwordTest))
        assertEquals(false, user.status)
        assertEquals("Wrong username or password, please try again.", user.message)
    }

    @Test
    fun `status equals true when check valid account with right username or password `() {
        val usernameTest = "GoBear"
        val passwordTest = "GoBearDemo"
        val user = viewModel.checkValidAccount(Pair(usernameTest, passwordTest))
        assertEquals(true, user.status)
        assertEquals("Login success.", user.message)
    }

    @Test
    fun `login success`() {
        val usernameTest = "GoBear"
        val passwordTest = "GoBearDemo"
        val userTest = User(usernameTest, "$usernameTest$passwordTest")
        userTest.status = true

        val userLiveData = viewModel.userLiveData.testObserverObject()

        viewModel.login(usernameTest, passwordTest)
        assertEquals(userTest.username, (userLiveData.observedValues as User).username)
        assertEquals(userTest.token, (userLiveData.observedValues as User).token)
        assertEquals(userTest.status, (userLiveData.observedValues as User).status)
    }

    @Test
    fun `login fail with wrong username or password`() {
        val usernameTest = "test"
        val passwordTest = "123456"

        val userLiveData = viewModel.userLiveData.testObserverObject()

        viewModel.login(usernameTest, passwordTest)
        assertEquals(false, (userLiveData.observedValues as User).status)
    }
}