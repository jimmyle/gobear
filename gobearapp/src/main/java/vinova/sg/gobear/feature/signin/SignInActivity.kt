package vinova.sg.gobear.feature.signin

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.startActivity
import vinova.sg.app.Injector
import vinova.sg.app.base.BaseActivity
import vinova.sg.app.ext.showToast
import vinova.sg.app.utilities.Prefs
import vinova.sg.app.viewmodel.auth.AuthViewModel
import vinova.sg.gobear.R
import vinova.sg.gobear.databinding.ActivitySignInBinding
import vinova.sg.gobear.feature.home.MainActivity

class SignInActivity : BaseActivity() {

    private lateinit var binding: ActivitySignInBinding
    private lateinit var authViewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        edtUsername.requestFocus()
        getViewModels()
        bindEvents()
        setEvents()
    }

    private fun getViewModels() {
        authViewModel = getAuthViewModel()
        binding.viewModel = authViewModel
    }

    private fun bindEvents() {
        authViewModel.userLiveData.observe(this, Observer {
            if (it.status == true) {
                Prefs(this).saveData(it.username, it.token, binding.cbRememberMe.isChecked)
                startActivity<MainActivity>()
                showToast(it.message)
                finish()
            } else
                showToast(it.message)
        })
    }

    private fun setEvents() {
        edtPassword.setOnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                authViewModel.login(authViewModel.username.get() ?: "", authViewModel.password.get()
                        ?: "")
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun getAuthViewModel(): AuthViewModel {
        val factory = Injector.provideAuthViewModelFactory()
        return ViewModelProviders.of(this, factory)
                .get(AuthViewModel::class.java)
    }
}
