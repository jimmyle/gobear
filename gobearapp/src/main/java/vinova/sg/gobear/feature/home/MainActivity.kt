package vinova.sg.gobear.feature.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import vinova.sg.app.Injector
import vinova.sg.app.base.BaseActivity
import vinova.sg.app.ext.gone
import vinova.sg.app.ext.showToast
import vinova.sg.app.ext.visible
import vinova.sg.app.repository.features.RssRepository
import vinova.sg.app.utilities.Prefs
import vinova.sg.app.utilities.setupToolbar
import vinova.sg.app.viewmodel.rss.GetNewsViewModel
import vinova.sg.gobear.R
import vinova.sg.gobear.feature.signin.SignInActivity

class MainActivity : BaseActivity() {

    private lateinit var newsViewModel: GetNewsViewModel
    private lateinit var adapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar(this, "BBC News")

        progressBar.visible()
        newsViewModel = getNewsViewModel()
        initAdapter()
        initSwipeToRefresh()
        bindEvents()
    }

    private fun initAdapter() {
        adapter = NewsAdapter { newsViewModel.getRssData() }
        rvNews.adapter = adapter
    }

    private fun initSwipeToRefresh() {
        swRefresh.setOnRefreshListener {
            swRefresh.isRefreshing = true
            newsViewModel.getRssData()
        }
    }

    private fun bindEvents() {
        newsViewModel.getRssData()

        newsViewModel.newsLiveData.observe(this, Observer {
            swRefresh.isRefreshing = false
            adapter.submitList(it)
            progressBar.gone()
        })

        newsViewModel.errorLiveData.observe(this, Observer {
            showToast(it?.message)
        })

        newsViewModel.networkState.observe(this, Observer {
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_logout) {
            Prefs(this).clearAllData()
            startActivity<SignInActivity>()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getNewsViewModel(): GetNewsViewModel {
        val repoType = RssRepository.Type.IN_MEMORY_BY_OBJECT
        val factory = Injector.provideServiceViewModelFactory(this, repoType)
        return ViewModelProviders.of(this, factory)
                .get(GetNewsViewModel::class.java)
    }
}
