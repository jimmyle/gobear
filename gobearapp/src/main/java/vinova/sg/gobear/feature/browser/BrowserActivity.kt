package vinova.sg.gobear.feature.browser

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_browser.*
import vinova.sg.app.ext.gone
import vinova.sg.app.ext.visible
import vinova.sg.app.utilities.setupToolbar
import vinova.sg.gobear.R

class BrowserActivity : AppCompatActivity() {

    companion object {
        const val KEY_TITLE = "title"
        const val KEY_URL = "url"

        fun intentFor(context: Context, title: String, url: String?): Intent =
                Intent(context, BrowserActivity::class.java).apply {
                    putExtra(KEY_TITLE, title)
                    putExtra(KEY_URL, url)
                }
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setupToolbar(this, intent.getStringExtra(KEY_TITLE), true)

        progressBar.visible()

        webView.apply {
            settings.javaScriptEnabled = true
            settings.loadsImagesAutomatically = true
            scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            loadUrl(intent.getStringExtra(KEY_URL) ?: return@apply)
        }

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar.gone()
                super.onPageFinished(view, url)
            }

        }
    }
}
