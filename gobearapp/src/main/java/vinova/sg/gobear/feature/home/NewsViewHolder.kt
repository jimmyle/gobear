package vinova.sg.gobear.feature.home

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import vinova.sg.app.model.feature.news.News
import vinova.sg.app.utilities.GlideApp
import vinova.sg.gobear.databinding.ItemNewsBinding
import vinova.sg.gobear.feature.browser.BrowserActivity

class NewsViewHolder(view: View, private val binding: ItemNewsBinding) : RecyclerView.ViewHolder(view) {
    companion object {
        fun create(binding: ItemNewsBinding): NewsViewHolder = NewsViewHolder(binding.root, binding)
    }

    fun bind(item: News?) {
        binding.news = item

        GlideApp.with(itemView.context).load(item?.image)
                .centerCrop()
                .into(binding.ivNews)

        binding.clContainer.setOnClickListener {
            it.context.startActivity(BrowserActivity.intentFor(it.context, "News", item?.link))
        }

        binding.executePendingBindings()
    }
}