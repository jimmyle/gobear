package vinova.sg.gobear.feature.splashscreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import vinova.sg.app.utilities.Prefs
import vinova.sg.gobear.feature.home.MainActivity
import vinova.sg.gobear.feature.signin.SignInActivity

class SplashedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        if (!isAuthenticated())
            startActivity<SignInActivity>()
        else
            startActivity(intentFor<MainActivity>())

        super.onCreate(savedInstanceState)
        finish()
    }

    private fun isAuthenticated(): Boolean {
        return Prefs(this).isRemember == true
    }
}