package vinova.sg.gobear.feature.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import vinova.sg.app.model.feature.news.News
import vinova.sg.app.repository.NetworkState
import vinova.sg.gobear.R
import vinova.sg.gobear.base.BaseNetworkStateItemViewHolder
import vinova.sg.gobear.databinding.ItemNewsBinding

class NewsAdapter(private val retryCallback: () -> Unit) :
        PagedListAdapter<News, RecyclerView.ViewHolder>(CART_COMPARATOR) {
    companion object {
        val CART_COMPARATOR = object : DiffUtil.ItemCallback<News>() {
            override fun areContentsTheSame(oldItem: News, newItem: News): Boolean =
                    oldItem.title == newItem.title

            override fun areItemsTheSame(oldItem: News, newItem: News): Boolean =
                    oldItem.title == newItem.title

            override fun getChangePayload(oldItem: News, newItem: News): Any? {
                return null
            }
        }
    }

    private lateinit var binding: ItemNewsBinding
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            when (viewType) {
                R.layout.network_state_item -> BaseNetworkStateItemViewHolder.create(parent, retryCallback)

                R.layout.item_news -> {
                    binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                            R.layout.item_news, parent, false)
                    NewsViewHolder.create(binding)
                }

                else -> throw IllegalArgumentException("unknown view type $viewType")
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_news -> (holder as NewsViewHolder).bind(getItem(position))
            R.layout.network_state_item -> (holder as BaseNetworkStateItemViewHolder).bindTo(
                    networkState)
        }
    }

    override fun getItemViewType(position: Int): Int = when {
        hasExtraRow() && position == itemCount - 1 -> R.layout.network_state_item
        else -> R.layout.item_news
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemCount(): Int = super.getItemCount() + if (hasExtraRow()) 1 else 0

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }
}