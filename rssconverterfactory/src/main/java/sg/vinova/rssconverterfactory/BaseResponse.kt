package sg.vinova.rssconverterfactory

abstract class BaseResponse(var status: Boolean? = null, var message: String? = null, var code: Int? = null) {
    fun isSuccess() = status == true
}